'''
    Trabalho desenvolvido pela equipe:
    Beatriz Correia Santos
    Gabriel Silva da Rocha
    Karina Lima e Lima
'''

import time
import unittest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select


class TestSauceDemoPages(unittest.TestCase):

    browser = Chrome()

    def test_ct01(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Products', titulo)

    def test_ct02(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Epic sadface: Password is required', error_msg)

    def test_ct03(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_use")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Epic sadface: Username and password do not match any user in this service', error_msg)

    def test_ct04(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_use")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_saucee")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Epic sadface: Username and password do not match any user in this service', error_msg)

    def test_ct05(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_saucee")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Epic sadface: Username is required', error_msg)

    def test_ct06(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        botao_menu = self.browser.find_element(By.ID, "react-burger-menu-btn")
        botao_menu.click()

        time.sleep(0.1)

        botao_logout = self.browser.find_element(By.LINK_TEXT, 'Logout')
        botao_logout.click()

        titulo_login = self.browser.find_element(By.CLASS_NAME, "login_logo").text
        print(titulo_login)
        self.assertEqual('Swag Labs', titulo_login)


    def test_ct07(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        botao_addcart = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
        botao_addcart.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('1', cart_number)

    def test_ct08(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        # botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
        # botao_addcart_backpack.click()

        botao_addcart_bike_light = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-bike-light")
        botao_addcart_bike_light.click()

        botao_addcart_t_shirt = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-bolt-t-shirt")
        botao_addcart_t_shirt.click()

        botao_addcart_jacket = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-fleece-jacket")
        botao_addcart_jacket.click()

        botao_addcart_onesie = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-onesie")
        botao_addcart_onesie.click()

        botao_addcart_t_shirt_red = self.browser.find_element(By.ID, "add-to-cart-test.allthethings()-t-shirt-(red)")
        botao_addcart_t_shirt_red.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('6', cart_number)

    def test_ct09(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        botao_remove_backpack = self.browser.find_element(By.ID, "remove-sauce-labs-backpack")
        botao_remove_backpack.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('5', cart_number)

    def test_ct10(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        # botao_remove_backpack = self.browser.find_element(By.ID, "remove-sauce-labs-backpack")
        # botao_remove_backpack.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('5', cart_number)

        botao_remove_bike_light = self.browser.find_element(By.ID, "remove-sauce-labs-bike-light")
        botao_remove_bike_light.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('4', cart_number)

        botao_remove_t_shirt = self.browser.find_element(By.ID, "remove-sauce-labs-bolt-t-shirt")
        botao_remove_t_shirt.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('3', cart_number)

        botao_remove_jacket = self.browser.find_element(By.ID, "remove-sauce-labs-fleece-jacket")
        botao_remove_jacket.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('2', cart_number)

        botao_remove_onesie = self.browser.find_element(By.ID, "remove-sauce-labs-onesie")
        botao_remove_onesie.click()

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('1', cart_number)

        botao_remove_t_shirt_red = self.browser.find_element(By.ID, "remove-test.allthethings()-t-shirt-(red)")
        botao_remove_t_shirt_red.click()

        try:
            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('', cart_number)
        except:
            pass
            # self.assertEqual()

    def test_ct11(self):
        # browser = Chrome()
        addCarrinho = self.browser.find_elements(By.XPATH, "//button[contains(text(), 'Add to cart')]")
        for clicandoAdd in addCarrinho:
            clicandoAdd.click()

        opcoes = self.browser.find_element(By.XPATH, '//*[@id="react-burger-menu-btn"]')
        opcoes.click()

        time.sleep(0.5)
        resetAppState = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="reset_sidebar_link"]')))
        resetAppState.click()

        closeX = self.browser.find_element(By.XPATH, '//*[@id="react-burger-cross-btn"]')
        closeX.click()

        try:
            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('', cart_number)
        except:
            pass
            # self.assertEqual()

    def test_ct12(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        botao_addcart = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
        botao_addcart.click()# click 1
        botao_addcart.click()# click 2

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('2', cart_number)


    def test_ct13(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        select = Select(self.browser.find_element(By.CLASS_NAME, "product_sort_container"))

        # select by visible text
        select.select_by_visible_text('Name (Z to A)')

        items = self.browser.find_elements(By.CLASS_NAME, "inventory_item_name")
        items_list_text = list()
        items_list_text_sorted = list()
        for item in items:
            items_list_text.append(item.text)
            items_list_text_sorted.append(item.text)
            print("ITENS: ", item.text)

        items_list_text_sorted = sorted(items_list_text_sorted, reverse=True)

        for index in range(len(items_list_text)):
            self.assertEqual(items_list_text_sorted[index], items_list_text[index])
            print(items_list_text_sorted[index], items_list_text[index])

    def test_ct14(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        select = Select(self.browser.find_element(By.CLASS_NAME, "product_sort_container"))

        # select by visible text
        select.select_by_visible_text('Name (A to Z)')

        items = self.browser.find_elements(By.CLASS_NAME, "inventory_item_name")
        items_list_text = list()
        items_list_text_sorted = list()
        for item in items:
            items_list_text.append(item.text)
            items_list_text_sorted.append(item.text)
            print("ITENS: ", item.text)

        items_list_text_sorted = sorted(items_list_text_sorted)

        for index in range(len(items_list_text)):
            self.assertEqual(items_list_text_sorted[index], items_list_text[index])
            print(items_list_text_sorted[index], items_list_text[index])

    def test_ct15(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        select = Select(self.browser.find_element(By.CLASS_NAME, "product_sort_container"))

        # select by visible text
        select.select_by_visible_text('Price (low to high)')

        items_price = self.browser.find_elements(By.CLASS_NAME, "inventory_item_price")
        items_price_list_text = list()
        items_price_list_text_sorted = list()
        for item in items_price:
            items_price_list_text.append(float(item.text[1:]))
            items_price_list_text_sorted.append(float(item.text[1:]))
            print("ITENS: ", item.text)

        items_price_list_text_sorted = sorted(items_price_list_text_sorted)

        for index in range(len(items_price_list_text)):
            self.assertEqual(items_price_list_text_sorted[index], items_price_list_text[index])
            print(items_price_list_text_sorted[index], items_price_list_text[index])

    def test_ct16(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        select = Select(self.browser.find_element(By.CLASS_NAME, "product_sort_container"))

        # select by visible text
        select.select_by_visible_text('Price (high to low)')

        items_price = self.browser.find_elements(By.CLASS_NAME, "inventory_item_price")
        items_price_list_text = list()
        items_price_list_text_sorted = list()
        for item in items_price:
            items_price_list_text.append(float(item.text[1:]))
            items_price_list_text_sorted.append(float(item.text[1:]))
            print("ITENS: ", item.text)

        items_price_list_text_sorted = sorted(items_price_list_text_sorted, reverse=True)

        for index in range(len(items_price_list_text)):
            self.assertEqual(items_price_list_text_sorted[index], items_price_list_text[index])
            print(items_price_list_text_sorted[index], items_price_list_text[index])

    def test_ct17(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        linkBolsa = self.browser.find_element(By.XPATH, '//*[@id="item_4_title_link"]')
        linkBolsa.click()

        time.sleep(1)

        titulo_backpack = self.browser.find_element(By.XPATH, "//*[@id='inventory_item_container']/div/div/div[2]/div[1]").text
        self.assertEqual('Sauce Labs Backpack', titulo_backpack)

    def test_ct18(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        linkBolsa = self.browser.find_element(By.XPATH, '//*[@id="item_4_title_link"]')
        linkBolsa.click()

        time.sleep(1)

        try:
            procurandoAdd = self.browser.find_element(By.XPATH, '//button[contains(text(), "Add to cart")]')
            procurandoAdd.click()

            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('1', cart_number)
        except:
            print("Item já está no carrinho")

            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('1', cart_number)

    def test_ct19(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        linkBolsa = self.browser.find_element(By.XPATH, '//*[@id="item_4_title_link"]')
        linkBolsa.click()

        time.sleep(1)

        try:
            procurandoRemove = self.browser.find_element(By.XPATH, '//button[contains(text(), "Remove")]')
            procurandoRemove.click()

            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('', cart_number)
        except:
            # self.assertEqual('', cart_number)
            pass

    def test_ct20(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        linkBolsa = self.browser.find_element(By.XPATH, '//*[@id="item_4_title_link"]')
        linkBolsa.click()

        time.sleep(1)

        try:
            backProducts = self.browser.find_element(By.XPATH, '//*[@id="back-to-products"]')
            backProducts.click()

            titulo = self.browser.find_element(By.CLASS_NAME, "title").text
            print(titulo)
            self.assertEqual('Products', titulo)
        except:
            titulo = self.browser.find_element(By.CLASS_NAME, "title").text
            print(titulo)
            self.assertEqual('Products', titulo)

    def test_ct21(self):
        # browser = Chrome()
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        self.assertEqual('Your Cart', titulo)

    def test_ct22(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        campo_quantidade = self.browser.find_element(By.CLASS_NAME, "cart_quantity")
        campo_quantidade.send_keys(Keys.BACKSPACE, "2")  # tentativa de adicionar dois itens

        cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
        self.assertEqual('2', cart_number)

    def test_ct23(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        removeCarrinho = self.browser.find_elements(By.XPATH, "//button[contains(text(), 'Remove')]")
        for clicandoRemove in removeCarrinho:
            clicandoRemove.click()

        try:
            cart_number = self.browser.find_element(By.CLASS_NAME, "shopping_cart_badge").text
            self.assertEqual('', cart_number)
        except:
            # self.assertEqual('', cart_number)
            pass

    def test_ct24(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        continueShopping = self.browser.find_element(By.XPATH, '//*[@id="continue-shopping"]')
        continueShopping.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Products', titulo)

    def test_ct25(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Checkout: Your Information', titulo)

    def test_ct26(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: Last Name is required', error_msg)


    def test_ct27(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("abc")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: First Name is required', error_msg)

    def test_ct28(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("abc")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: First Name is required', error_msg)

    def test_ct29(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: Postal Code is required', error_msg)

    def test_ct30(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: Last Name is required', error_msg)

    def test_ct31(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: First Name is required', error_msg)

    def test_ct32(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        error_msg = self.browser.find_element(By.CSS_SELECTOR, "*[data-test=\"error\"]").text
        print(error_msg)
        self.assertEqual('Error: First Name is required', error_msg)

    def test_ct33(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Checkout: Overview', titulo)

    def test_ct34(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()
        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        button_cancel = self.browser.find_element(By.XPATH, '//*[@id="cancel"]')
        button_cancel.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Your Cart', titulo)

    def test_ct35(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        quantity_items = self.browser.find_elements(By.CLASS_NAME, "cart_item_label")
        print(quantity_items)
        self.assertEqual(1, len(quantity_items))


    def test_ct36(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        item_price = self.browser.find_element(By.CLASS_NAME, "summary_subtotal_label").text
        print(item_price)
        indice = item_price.index("$")
        item_price_float = float(item_price[indice+1:])
        print(item_price_float)

        tax_price = self.browser.find_element(By.CLASS_NAME, "summary_tax_label").text
        print(tax_price)
        indice = tax_price.index("$")
        tax_price_float = float(tax_price[indice+1:])

        print(tax_price_float)

        total_price_calc = item_price_float + tax_price_float


        total_price = self.browser.find_element(By.CLASS_NAME, "summary_total_label").text
        print(total_price)
        indice = total_price.index("$")
        total_price_float = float(total_price[indice+1:])

        print(total_price_float)


        self.assertEqual(total_price_calc, total_price_float)

    def test_ct37(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        button_cancel = self.browser.find_element(By.XPATH, '//*[@id="cancel"]')
        button_cancel.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Checkout: Your Information', titulo)


    def test_ct38(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        button_finish = self.browser.find_element(By.XPATH, '//*[@id="finish"]')
        button_finish.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Checkout: Complete!', titulo)

    def test_ct39(self):

        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        removeCarrinho = self.browser.find_elements(By.XPATH, "//button[contains(text(), 'Remove')]")
        for clicandoRemove in removeCarrinho:
            clicandoRemove.click()

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertNotEqual('Checkout: Your Information', titulo)


    def test_ct40(self):
        self.browser.get('https://www.saucedemo.com/')
        campo_texto_user = self.browser.find_element(By.ID, "user-name")
        campo_texto_user.send_keys("standard_user")

        campo_texto_passw = self.browser.find_element(By.ID, "password")
        campo_texto_passw.send_keys("secret_sauce")

        botao_login = self.browser.find_element(By.ID, "login-button")
        botao_login.click()

        try:
            botao_addcart_backpack = self.browser.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
            botao_addcart_backpack.click()

        except:
            print("ITEM JA ADICIONADO AO CARRINHO")
            pass

        clickCarrinho = self.browser.find_element(By.XPATH, '//*[@id="shopping_cart_container"]/a')
        clickCarrinho.click()

        time.sleep(1)

        button_checkout = self.browser.find_element(By.XPATH, '//*[@id="checkout"]')
        button_checkout.click()

        campo_first_name = self.browser.find_element(By.ID, "first-name")
        campo_first_name.send_keys("abc")

        campo_last_name = self.browser.find_element(By.ID, "last-name")
        campo_last_name.send_keys("wxyz")

        campo_zipcode = self.browser.find_element(By.ID, "postal-code")
        campo_zipcode.send_keys("1234")

        button_continue = self.browser.find_element(By.XPATH, '//*[@id="continue"]')
        button_continue.click()

        button_finish = self.browser.find_element(By.XPATH, '//*[@id="finish"]')
        button_finish.click()

        button_back_products = self.browser.find_element(By.XPATH, '//*[@id="back-to-products"]')
        button_back_products.click()

        titulo = self.browser.find_element(By.CLASS_NAME, "title").text
        print(titulo)
        self.assertEqual('Products', titulo)

if __name__ == '__main__':
    unittest.main()